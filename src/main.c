#include <stm32f103xb.h>
#include "can.h"
#include "spi_slave.h"
#include "chip_options.h"
#include "systick.h"
#include "gpio.h"
#include "nocan.h"
#include "usart.h"

void init_clock_HSE_72MHz(void)
{
    // Conf clock : 72MHz using HSE 8MHz crystal w/ PLL X 9 (8MHz x 9 = 72MHz)
    FLASH->ACR |= FLASH_ACR_LATENCY_2; // 1. Two wait states, per datasheet
    RCC->CFGR |= RCC_CFGR_PPRE1_2;     // 2. prescale AHB1 = HCLK/2
    RCC->CR |= RCC_CR_HSEON;           // 3. enable HSE clock
    while (!(RCC->CR & RCC_CR_HSERDY))
        ; // 4. wait for the HSEREADY flag

    RCC->CFGR |= RCC_CFGR_PLLSRC;   // 5. set PLL source to HSE
    RCC->CFGR |= RCC_CFGR_PLLMULL9; // 6. multiply by 9
    RCC->CR |= RCC_CR_PLLON;        // 7. enable the PLL
    while (!(RCC->CR & RCC_CR_PLLRDY))
        ; // 8. wait for the PLLRDY flag

    RCC->CFGR |= RCC_CFGR_SW_PLL; // 9. set clock source to pll

    while (!(RCC->CFGR & RCC_CFGR_SWS_PLL))
        ; // 10. wait for PLL as source

    SystemCoreClockUpdate(); // 11. calculate the SYSCLOCK value
}

void init_clock_HSE_48MHz(void)
{
    // Conf clock : 48MHz using HSE 8MHz crystal w/ PLL X 6 (8MHz x 6 = 48MHz)
    FLASH->ACR |= FLASH_ACR_LATENCY_2; // 1. Two wait states, per datasheet
    RCC->CFGR |= RCC_CFGR_PPRE1_2;     // 2. prescale AHB1 = HCLK/2
    RCC->CR |= RCC_CR_HSEON;           // 3. enable HSE clock
    while (!(RCC->CR & RCC_CR_HSERDY))
        ; // 4. wait for the HSEREADY flag

    RCC->CFGR |= RCC_CFGR_PLLSRC;   // 5. set PLL source to HSE
    RCC->CFGR |= RCC_CFGR_PLLMULL6; // 6. multiply by 6
    RCC->CR |= RCC_CR_PLLON;        // 7. enable the PLL
    while (!(RCC->CR & RCC_CR_PLLRDY))
        ; // 8. wait for the PLLRDY flag

    RCC->CFGR |= RCC_CFGR_SW_PLL; // 9. set clock source to pll

    while (!(RCC->CFGR & RCC_CFGR_SWS_PLL))
        ; // 10. wait for PLL as source

    SystemCoreClockUpdate(); // 11. calculate the SYSCLOCK value
}

int main(void)
{
    int n = 0;
    char c;

    init_clock_HSE_48MHz();

    systick_init();

    usart_init(115200);
    usart_printf("\nSystemCoreClock=%u\n", SystemCoreClock);

    gpio_init();

    gpio_set_led();

    chip_options_configure();

    gpio_clear_led();

    spi_slave_init();

    nocan_open();

    usart_printf("\nStarting serial console.\n");
    for (;;)
    {
        if (usart_available())
        {
            c = usart_getc();
            switch (c)
            {
            case 'b':
                usart_printf("\nLed off");
                gpio_clear_led();
                break;
            case 'B':
                usart_printf("\nLed on");
                gpio_set_led();
                break;
            case 'd':
                usart_printf("\nregisters: ");
                for (int i = 0; i < sizeof(NOCAN_REGS); i++)
                    usart_printf("%x ", ((uint8_t *)&NOCAN_REGS)[i]);
                usart_printf("\nstat  : %x (%s)", NOCAN_REGS.STATUS, nocan_status_string());
                usart_printf("\nver   : %x", NOCAN_REGS.VERSION);
                usart_printf("\ntx/rx : %u/%u", NOCAN_REGS.CAN_TX_COUNT, NOCAN_REGS.CAN_RX_COUNT);
                break;
            case 'f':
                usart_printf("\nfilters: ");
                for (uint32_t f = 0; f < CAN_FILTER_COUNT; f++)
                {
                    usart_printf("\n%x %x %x: ",
                                 can_filter_buffer[3 * f],
                                 can_filter_buffer[3 * f + 1],
                                 can_filter_buffer[3 * f + 2]);
                }
                break;
            case 'h':
                usart_printf("\ncommands: [b] set led off/on [c] send address request [f] dump can filters [h] help, [i] read chip uid, [d] dump registers, [p] 1 second systick pause [r] reset SAMD21 [z] reset stm32f0.");
                break;
            case 'i':
                usart_printf("\nChip UID: ");
                for (int i = 0; i < 12; i++)
                    usart_printf("%x ", CHIP_UDID[i]);
                break;
            case 'c':
                usart_printf("\nconnecting: ");
                if (can_sys_send(0, NOCAN_SYS_ADDRESS_REQUEST, 0, NOCAN_REGS.UDID, 8) < 0)
                    usart_printf("FAIL");
                else
                    usart_printf("OK");
                break;
            case 'p':
                usart_printf("\npause: ");
                systick_delay(1000);
                usart_printf("OK");
                break;
            case 'r':
                gpio_clear_led();
                usart_printf("\nreseting SAMD21");
                gpio_clear_reset();
                for (uint32_t count = 0; count < 10000; count++)
                    asm("nop");
                gpio_set_reset();
                break;
            case 'z':
                usart_printf("\nreseting self (stm32f0)...");
                NVIC_SystemReset();
                break;
            }
            usart_printf("\n[h,b,B,c,d,f,i,q,r,z]?");
        }
    }
}

/*
 * Debug help -- if asserts are enabled, assertion failure
 * from standard peripheral  library ends up here 
 */

#ifdef USE_FULL_ASSERT
void assert_failed(uint8_t *file, uint32_t line)
{
    /* Infinite loop */
    /* Use GDB to find out why we're here */
    while (1)
    {
    }
}
#endif
