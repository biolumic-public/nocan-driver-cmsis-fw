#include <stm32f103xb.h>
#include "systick.h"
#include "usart.h"
#include "systick.h"

static __IO uint32_t TimingDelay;
static __IO uint32_t Now;


void ms_delay(int ms)
{
   while (ms-- > 0) {
      volatile int x=500;
      while (x-- > 0)
         __asm("nop");
   }
}

void systick_init()
{
    // SystemCoreClockUpdate();

    Now = 0;

    if (SysTick_Config(SystemCoreClock / 1000)) for(;;);

    // usart is not ready yet.
    // usart_printf("\nSystemCoreClock=%u\n",SystemCoreClock);
}

void systick_delay(uint32_t delay_ms)
{
    // TimingDelay = delay_ms;
    // while(TimingDelay != 0);

    ms_delay(delay_ms);
}

uint32_t systick_now()
{
    return Now;
}

void SysTick_Handler(void)
{
    if (TimingDelay != 0x00)
        TimingDelay --;
    Now++;
}

void active_delay(uint32_t delay_ms)
{
    uint32_t count = delay_ms * 4000;
    while (count-- > 0)
        asm("nop");
}

