#include <stm32f103xb.h>
#include "stm32f1_helpers.h"
#include "gpio.h"

/*
 * CAN_RX_INT: PA0
 * CAN_TX_INT: PA1
 * LED: PB13
 * RESET: PB8-BOOT0
 */

void gpio_init(void)
{
  // enable clock on GPIOA
    #ifdef STM32F103xB
    RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
    #else
    RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
    #endif

  // Configure pins on GPIOA
  GPIO_CONFIGURE_OUTPUT(GPIOA, CAN_TX_INT_Pin, GPIO_SPEED_LOW, GPIO_PUSH_PULL);
  GPIO_CONFIGURE_OUTPUT(GPIOA, CAN_RX_INT_Pin, GPIO_SPEED_LOW, GPIO_PUSH_PULL);
 
  // enable clock on GPIOB
    #ifdef STM32F103xB
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
    #else
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
    #endif

  // configure pins on GPIOB
  GPIO_CONFIGURE_OUTPUT(GPIOB, CAN_LED_Pin, GPIO_SPEED_LOW, GPIO_PUSH_PULL);
  GPIO_CONFIGURE_OUTPUT(GPIOB, RESET_Pin, GPIO_SPEED_LOW, GPIO_OPEN_DRAIN);

  // set reset to high
  gpio_set_reset();

  // set led low
  gpio_clear_led();

}



