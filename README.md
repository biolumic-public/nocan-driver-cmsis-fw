
# NoCAN Core Driver for STM32F103xB

This repository contains source code for the STM32F103xB, which acts as a CAN bus
driver for NoCAN compatible nodes.  Learn more about the NoCAN platform by
visiting [https://omzlo.com/articles/introduction-to-the-nocan-platform](https://omzlo.com/articles/introduction-to-the-nocan-platform).

This work is a fork of [https://github.com/omzlo/canzero-driver-firmware](https://github.com/omzlo/canzero-driver-firmware).


## Motivation

At the time of writing there is an ongoing silicon shortage which is resulting in
long lead times for microcontrollers such as the STM32F042.  In fact almost all
STM32F0* microcontrollers are unavailable.  The STM32F103 availability was better
so we have ported the source code to suit this microcontroller.


## Build System

This repo is a PlatformIO project.  The code can be built by folllowing these steps:

1. Install PlatformIO by following this guide: [https://platformio.org/install/ide?install=vscode](https://platformio.org/install/ide?install=vscode)
2. Clone the repo with `git clone <repo/url>`
3. Build and upload the code with `platformio upload`


## MCU Support

Currently the only supported microcontroller is the STM32F103xB.  Very little testing
has be done.  More testing will be carried out once we have PCB's in hand.

Support for the STM32F042, as used in the CANZERO, will be added when time allows.
Support for other STM32 microcontrollers will be added if/when needed.


## MCU Pin Configurations

### STM32F103xB

| Signal Name       | Pin Name         |
| ----------------- | ---------------- |
| CAN_RX            | PB08             |
| CAN_TX            | PB09             |
| DEBUG_RX          | PA02             |
| DEBUG_TX          | PA03             |
| CAN_MOSI          | PA07             |
| CAN_MISO          | PA06             |
| CAN_TX_INT        | PA01             |
| CAN_RX_INT        | PA00             |
| CAN_SS            | PA04             |
| CAN_SCK           | PA05             |
| RST               | PB01             |
| CAN_LED           | PB13             |

Connect 8MHz crystal to PD0 and PD1
